.. KSSOLV2.0 documentation master file, created by
   sphinx-quickstart on Mon Jul 18 12:48:45 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to KSSOLV2.0 documentation!
==================================

Contents:

.. toctree::
   :maxdepth: 1

   intro
   start
   function
   classes 
   algorithm
   pseudopotential
   viz
   tools
   examples
   test
   refs

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

