Pseudo-potential
==================================

KSSOLV uses pseudo-potentials to model nuclear potentials experienced by
electrons.  By default, the Optimized Norm-Conserving Vanderbilt (ONCV) pseudo-potentials are used. One can also specify this choice by using the following command

.. code-block:: matlab

   >> kssolvpptype('default');


It is also possible to use other types of pseudo-potentials. To do that,
users should first download pseudo-potential files for desired atoms
to the folder :attr:`PATH_TO_KSSOLV/ppdata` or create subfolders in this folder. Currently,
KSSOLV supports two file formats for pseudo-potential, i.e., `UPF
<https://esl.cecam.org/Unified_Pseudopotential_Format_(UPF)>`_
and `PSP8 <https://docs.abinit.org/developers/psp8_info/>`_. The
pseudo-potential file name should follow the convention as
:attr:`asym+sep+pptype+*+.ext`, where :attr:`asym` denotes the atom
symbol, :attr:`sep` denotes the separator (`_` or `-`), :attr:`pptype`
denotes the type of pseudo-potential, :attr:`ext` denotes the file
extension, and :attr:`*` means any string. Examples of such filenames
include :attr:`Ag_ONCV_PBE-1.0.upf` and :attr:`Si.pbe-hgh.psp8`. Once
the pseudo-potential files are prepared, users can set the
:attr:`pptype` and :attr:`ext` parameters through :func:`kssolvpptype` function.
KSSOLV then adopt these imported pseudo-potential files instead of
those default ones.

************************************
**Why Pseudo-potential?**
************************************

The electrons of each atom are divided into two group. The 
electrons that fill up inner shells are called *core* electrons, whereas 
electrons in the outermost (major) shell that is not completely filled are called
*valence* electrons. For example, Silicon has 14 electrons. Its electron 
configuration is commonly written as :math:`1s^2 2s^2 2p^6 3s^2 3p^2`. The two electons in the 1s and 2s orbtials as wells as the six electrons in the 2p orbital are all core electrons that fill up the first and second major shells. The two electrons in the :math:`3s` orbital and two electrons in the :math:`3p` orbtial are valence electron because the third shell is partially filled.

Because core electrons of an atom are inert (chemically less active), 
they are combined with the nucleus to form an *ionic core*. This allows us 
to focus on the valence electrons which are more important in determining 
the chemical properties of a polyatomic system. Furthermore, the use of 
pseudo-potential allows us to remove the singularity at the origin of the 
nuclear potential :math:`V(r) = Z/|r|`, where :math:`Z` is the atomic 
charge.

************************************
**Pseudo-potential Construction**
************************************

We now sketch the basic steps involved in constructing an atomic pseudo-potential. The total pseudo-potential of a polyatomic system (molecules or solids) is the sum of all atomic pseudo-potentials associated with all atoms in the system and must account for the positions of the atoms.

Let :math:`n_{\mathrm{core}}` be the number of core electrons in an atom of interest, and :math:`R_{n_{\mathrm{core}}+i}(r)` be the radial part of the *i* th valence electron orbital of 
an atom centered at the origin of a 3D coordinate system with nuclear charge :math:`Z`, i.e., :math:`R_{n_{\mathrm{core}}+i}(r)` is the radial part
of the eigenfunction of the atomic Hamiltonian :math:`H_a = -\nabla^2 - Z/r` (in atomic units) associated with the eigenvalue :math:`\varepsilon_{n_{\mathrm{core}}+i}`.  
We construct a pseudo-potential :math:`V_{p}(r)` such that
the radial part of the *i* th eigenfunction of the pseudo-Hamiltonian :math:`H_p = -\nabla^2 - V_{p}(r)`, which we denote by :math:`\tilde{R}_i(r)`, matches :math:`R_{n_{\mathrm{core}}+i}(r)` for :math:`|r| > r_c`, where :math:`r_c` is a cutoff radius. Within the cutoff radius, :math:`\tilde{R}_i(r)` is a parametrized function that satisfies a number of norm conservation and smoothness constrains. For example,

.. math:: \int_{0}^{r_c} |\tilde{R}_i(r)|^2 dr = \int_{0}^{r_c} |R_{n_{\mathrm{core}}+i}(r)|^2 dr, 

or more generally,

.. math:: \int_{0}^{r_c} \tilde{R}_i(r)\tilde{R}_j(r)^2 dr = \int_{0}^{r_c} R_{n_{\mathrm{core}}+i}(r) R_{n_{\mathrm{core}}+j} dr.

The functions :math:`\tilde{R}_i(r)` and :math:`\tilde{R}_j(r)` are required to be orthogonal for :math:`i\neq j`.

There are a number of ways to parameterize :math:`\tilde{R}_i` and obtain the values of the parameters by imposing different types of constraints. The most well known is the Troullier-Martins scheme []. But there are many others. Hence a large variety of pseudo-potentials one can choose from.

Once :math:`\tilde{R}_i` is determined, we can compute the *i* th component of the atomic pseudo-potential :math:`V_{p}^{(i)}(r)` by solving the following problem

.. math:: \left[-\nabla^2 - V_{p}^{(i)}(r) \right]\tilde{\phi}_i = \varepsilon_i \tilde{\phi}_i,

where :math:`\tilde{\phi}_i` is the *i* th eigenfunction of :math:`H_p` with :math:`\tilde{R}_i` as its radial part. The solution can be written as a function in the form

.. math :: V_{p}^{(i)}(r) = \left[ -\nabla^2 \tilde{\phi}_i - \varepsilon_i \tilde{\phi}_i \right]/ \tilde{\phi}_i

or an operator in the form

.. math:: V_{p}^{(i)}(r) = | -\nabla^2 \tilde{\phi}_i - \varepsilon_i \tilde{\phi}_i \rangle \langle \tilde{\phi}_i |,

where we have used bra :math:`| \cdot \rangle` and ket :math:`\langle \cdot |` 
notation to distinguish a vector and its transpose.

The latter form is convenient when we combine all components into a single atomic total pseudo-potential 

.. math:: V_{p}(r) = \sum_i V_{p}^{(i)}(r) =  \sum_i | -\nabla^2 \tilde{\phi}_i - \varepsilon_i \tilde{\phi}_i \rangle \langle \tilde{\phi}_i |.

This form can be symmetrized to yield

.. math:: V_{p}(r) = \sum_i V_{p}^{(i)}(r) =  \sum_i \frac{| \beta_i \rangle \langle \beta_i |}{ \langle \beta_i, \tilde{\phi}_i \rangle},

where :math:`\beta_i =  -\nabla^2 \tilde{\phi}_i - \varepsilon_i \tilde{\phi}_i`.  This is often known as the *Kleinmann and Bylander* (KB) form.  This low rank representation is convienent in practical computation, and is applicable in both real and reciprocal spaces.

Note that the index *i* used in the summation above corresponds to a set of quantum numbers associated with an eigenfunction of atomic Schrodinger operator. One of the indices in the summation can be taken out and used as a reference. We can use the function form of the corresponding :math:`V_{p}^{(i)}(r)` to define a local pseudo-potential in real space. Therefore, pseudo-potentials are often written as

.. math:: V_{p}(r) = V_{p}^{\mathrm{loc}}(r) + V_{p}^{\mathrm{nl}}(r),

where :math:`V_{p}^{\mathrm{nl}}(r)` is represented in KB form.

All atomic pseudo-potentials for different atoms must be combined to form the total pseudo-potential experienced by all valence electrons. We refer users to the developer's guide for details on how local and non-local pseudo-potentials are implemented in KSSOLV.
