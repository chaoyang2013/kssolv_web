Classes
==================================

KSSOLV2.0 preserves many objected oriented features of the previous
version of KSSOLV. It contains several data classes suitable for 
representing both atomic and electronic structures of molecules and solids.

.. toctree::
   :maxdepth: 1

   Classes/atom
   Classes/Ggrid
   Classes/molecule
   Classes/crystal 
   Classes/wavefun
   Classes/Blochwave
   Classes/ham
   Classes/Blochham

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
