Direct Constrained Minimization
===============================

In the KSDFT formalism, the ground state of an atomistic system can be 
obtained by solving the following constrained minimization problem

.. math:: \min_{X^\ast X = I} E_{tot}(X),

where :math:`X \in \mathbb{C}^{n \times n_e}` contains :math:`n_e` 
discretized single particle wavefunctions associated with valence
electrons, the total energy and :math:`E_{tot}(X)` contains several
components, i.e.,

.. math:: E_{tot}(X) = E_{kin}(X) + E_{ion}(X) + E_{Hartree}(X) + E_{xc}(X),

where :math:`E_{kin}(X)` and :math:`E_{ion}(X)` are the kinetic energy of 
electrons and ionic pseudopotential energy induced by the ionic cores (nuclei+core), :math:`E_{Hartree}` is the Hartree potential energy induced by electron-electron repulsion, and :math:`E_{xc}` is the exchange correlation potential 
that accounts for other many-body interactions of the electrons. 

Instead of working with the first order necessary condition of the optimization
problem which yields the Kohn-Sham nonlinear eigenvalue problem

.. math:: H_{ks}(X) = X \Lambda,

which can be solved by an accelerated SCF iteration discussion in 
a previous section, we can try to solve the constrained minimization problem
directly.

KSSOLV2.0 provides a direct minimization algorithm (DCM) that can be used 
to minimize the total energy functional directly. The DCM algorithm
is similar to a nonlinear conjugate gradient method in which the approximate
minimizer :math:`X^{(k)}` is updated as

.. math:: X^{(k+1)} = X^{(k)} G_1 + R^{(k)} G_2 + P^{(k-1)} G_3,

where :math:`R^{(k)}` is the projected gradient gradient of :math:`E_{tot}` 
along the tangent of the orthonormal constraint, and :math:`P^{(k-1)}` 
is the previous search direction.  The matrices :math:`G_1`, :math:`G_2`, :math:`G_3` are chosen to minimize :math:`E_{tot}` within the subspace 
spanned by :math:`\{ X^{(k)}, R^{(k)}, P^{(k-1)}\}` subject to the 
orthonormality constraint.  The subspace minimization problem, which 
is still nonlinear, can be solved by the SCF iteration. 

The subspace minimization problem does not need to be solved to full
accuracy when :math:`X^{(k)}` away from the minimizer. But the approximate 
solution should yields a reduction in :math:`E_{tot}`. To ensure that,
a trust region technique can be used in the constrained minimization to 
ensure :math:`E_{tot}` descreases monotonically. In KSSOLV2.0, this 
version of the direction constrained minimization is provided by the 
:attr:`trdcm` function.

