.. _algo_mixing:

Charge or potential mixing
==========================

.. list-table:: Mixing types and parameter supported by KSSOLV2.0
   :widths: 25 50 
   :header-rows: 1

   * - Mixing type
     - Mixing parameters
   * - 'simple'
     - betamix
   * - 'Anderson'
     - betamix, mixdim 
   * - 'Broyden1'
     - betamix, mixdim, brank 
   * - 'Broyden2'
     - betamix, mixdim, brank 
   * - 'Broyden'
     - betamix, mixdim, brank 
   * - 'Kerker'
     - betamix
   * - 'Pulay'
     - betamix, mixdim
   * - 'Pulay+Kerker'
     - betamix, mixdim

When the exchange-correlation functional is an explicit function of 
the charge density :math:`\rho`, (LDA or GGA), the ground state electron
density is a fixed point of the Kohn-Sham map :math:`f_{\mu}`, i.e., :math:`\rho` satisfies the nonlinear equation

.. math::  \rho = f_\mu (\rho).

Such a fixed point can be obtained by solving the above equation using
a quasi-Newton method which produces a sequence of approximations

.. math::  \rho^{(k+1)} = \rho^{(k)} - B_k r^{(k)},

where :math:`r^{(k)} = \rho^{(k)} - f_\mu (\rho^{(k)})` is the residual, 
and :math:`B_k` is an approximation to the inverse of the Jacobian of 
:math:`\rho - f_{\mu}(\rho)` at :math:`\rho^{(k)}`.

When :math:`B_k` is chosen to be :math:`B_k = \beta I`, where :math:`I` is
the identity and :math:`0 < \beta < 1`, :math:`\rho^{(k+1)}` is simply
a convex combination of :math:`\rho^{(k)}` and :math:`f_\mu (\rho^{(k)})`. This type of update is often known as *simple mixing* with :math:`\beta` being 
the mixing parameter.  It can be invoked by
setting the **mixtype** parameter in the input arguments of the **mixing** 
function to **'simple'**, and the **betamix** parameter (corresponding to 
:math:`\beta` ) to a small constant between 0 and 1.

For many problems, the use of simple mixing will make the SCF iteration
converge if the mixing parameter :math:`\beta` is sufficiently small. However, 
converge can be extremely slow. A more effective way to approximate 
the inverse of the Jacobian is to use a Broyden-like scheme that can be
written as

.. math::  B_{k+1} = \beta I + (S_k - \beta Y_k) Y_k^{\dagger}, 

where 

.. math::  S_k = (s_k, s_{k-1}, ..., s_{k-\ell}),  Y_k = (y_k, y_{k-1}, ..., y_{k-\ell}),

with :math:`s_k = \rho^{(k)}-\rho^{(k-1)}` and :math:`y_k = r^{(k)}-r^{(k-1)}`. Here :math:`Y_k^{\dagger}` denotes the pseudo-inverse of :math:`Y_k`.   
Such a Jacobian inverse approximation yields an updating formula:

.. math::  \rho^{(k+1)} = (\rho^{(k)} - S_k g) + \beta (r^{(k)} - Y_k g),

where :math:`g=Y_k^{\dagger}r^{(k)}`.  This type of update is known 
as the *Anderson* mixing.  It can be invoked by setting **mixtype** to 
**'anderson'** with an appropriately chosen mixing parameter **betamix**, 
which is set to 0.7 by default.
In addition to **betamix**, one can also specify the integer 
parameter **mixdim**, defines the maximum number of columns allowed in
:math:`S_k` and :math:`Y_k`, and corresponds to the value of $\ell$ above.
This parameter is set to 10 by default.

KSSOLV2.0 also provides the classical Broyden methods for approximating 
the Jacobian (Broyden I) and the inverse of the Jacobian (Broyden II). 
In the latter case, the approximation :math:`B_k` is updated by

.. math::  B_{k+1} = B_k + (S_k - B_k Y_k) Y_k^{\dagger}.

Note that the difference between the Broyden and Anderson methods is that 
in the Anderson method :math:`B_k` is always reset to :math:`\beta I` for 
each :math:`k`.

For metallic systems, a more appropriate approximation to the inverse 
of the Jacobian has the following representation in the reciprocal space,

.. math::  B_{k} = \frac{|G|^2}{|G|^2+4\pi \zeta},

for some screening costant :math:`\zeta`, where :math:`G` is a reciprocal
space grid point.  We will refer users to references[a,b,c] for details of such an approximation. The updating scheme associated 
with this type of approximation is known as *Kerker* mixing.

Finally, mixing can be applied to either the electron density or the potential 
that depends on the electron density. In KSSOLV2.0, the choice can be
made in the SCF function.
