Planewave discretization
========================

KSSOLV uses a planewave expansion to represent the Kohn-Sham eigenfunctions. 
For a :math:`R`-periodic Kohn-Sham Hamiltonian, it follows from the Bloch theorem that an eigenfunction :math:`\psi_k(r)` can be expressed as

.. math:: \psi_k(r) = e^{i k \cdot R} u_k(r), 
   :label: blochpsi

where :math:`k` is a reciprocal space vector in the Brillouin zone of the unit cell on which :math:`H` is defined, and :math:`u_k(r) = u_k(r+R)` is periodic, and can be further approximated as (a Fourier series):

.. math::  u_k(r) = \sum_{j=1}^{n_w} c_{k,j} e^{i g_j \cdot r},

where :math:`c_{k,j}` are the Fourier expansion coefficients, :math:`g_j` is a reciprocal space vector, and :math:`n_w` is the total number of expansion coefficients what determines the accuracy of the approximation.

As a result, :math:`\psi_k(r)` can be written (approximately) as 

.. math::  \psi_k(r) = \sum_{j=1}^{n_w} c_{k,j} e^{i (k + g_j) \cdot r}.

The choice of :math:`k` in :eq:`blochpsi` is determined by an additional boundary condition on multiple unit cells, and is discussed in :ref:`blochham_label`.

When the unit cell is sufficiently large, i.e., :math:`|R|` is large,
the Brillouin zone is sufficiently small that it can be approximated by 
a single point at the center of the zone (often referred to as the gamma point.)
As a result, we can leave out the :math:`k` index in the eigenfunction.

For an isolated system such as a molecule or nanocluster, which is not periodic, we can place the system in a ficticious supercell that is periodically replicated in space. When the supercell is sufficiently large, planewave expansion can be used to approximate eigenfunctions of a Kohn-Sham Hamitonian.

