
Regularization through partial occupation 
=========================================

For systems with :math:`n_e` valence electron pairs, the Kohn-Sham problem is
ill-posed if the multiplicity of the :math:`n_e` th eigenvalue of 
of the Kohn-Sham Hamiltonian :math:`H(\rho)` is larger than 1. The ill-posedness
of the problem can be seen from the ambiguity of definition of electron density

.. math:: \rho = 2 \sum_{i=1}^{n_e} |\psi_i|^2,

where :math:`\psi_i` is the :math:`i` th eigenfunction associated with the :math:`i` th eigenvalue :math:`\varepsilon_i`, ordered as :math:`\varepsilon_1 \leq \varepsilon_2 \leq \cdots \leq \varepsilon_n`.

The degeneracy or near degeneracy of eigenvalues at the Fermi level :math:`\mu` is a
feature of metallic systems that exhibit no gap or a tiny gap between 
the valence (occupied) and conducting (unoccupied) states.

Computationally, the ill-poseness of such problems makes it difficult for the standard SCF iteration to converge. One often experience the so-called "charge sloshing" problem where the largest magnitude of :math:`\rho` oscillates among 
a few spatial locations.

To overcome this problem, a regularization technique is used to modify the 
fixed point equation to be solved as

.. math:: \rho = f_\beta (\rho),

where :math:`f_\beta` is the Fermi-Dirac distribution function defined as

.. math:: f_\beta(t) = \frac{1}{1+e^{\beta (t-\mu)}}.

The parameter :math:`\beta` is a regularization parameter that is related
to the temperature :math:`T` in the original definition of the
Fermi-Dirac distribution via

.. math:: \beta = \frac{1}{k_B T},

where :math:`k_B` is the Boltzman constant.

At zero temperature, :math:`\beta = \infty`, and :math:`f_\beta` becomes
a step function that is 1 for :math:`t \leq \mu` and 0 for :math:`t > \mu`. This corresponds to the standard Kohn-Sham model in which all eigenvalues below
:math:`\mu` are occupied. If there is a gap between occupied and unoccupied
states, :math:`f_\beta(\varepsilon_i) = 1` for :math:`i = 1,2,...,n_e`, and
:math:`f_\beta(\varepsilon_i) = 0` for :math:`i > n_e`.

When :math:`T > 0` (often known as finite temperature in physics literature), 
:math:`0 \leq f_{\beta}(\varepsilon_i) \leq 1`, i.e., electronic states can be partially occupied.  In particular, it is possible
that :math:`f_{\beta}(\varepsilon_i) > 0` for :math:`i > n_e`. However,
as :math:`i \rightarrow n`, :math:`f_{\beta}(\varepsilon_i) \rightarrow 0`.

The Fermi-level :math:`\mu` is chosen to ensure :math:`\int \rho(r) dr = 2n_e`.

Note that Fermi-Dirac distribution is not the only way to regularize the 
Kohn-Sham problem.  The regularization does change the problem to be solved. However, since the original Kohn-Sham model is an approximation, such a modification is justified as long as the modified model is effective in applications 
such as geometry optimization and ab initio molecular dynamics.

The regularization introduces additional computation to be performed in the 
SCF iteration:

* We need to compute more than :math:`n_e` eigenpairs in the diagonalization procedure in order to evaluate the electron density as

.. math:: \rho = \sum_{i=1}^{k} f_\beta(\varepsilon_i) |\psi_i|^2,

for some :math:`k>n_e`.

* We need to determine :math:`\mu` by solving :math:`\int \rho(r) dr = 2n_e`. Because the left hand side is a monotonic function with respect to :math:`\mu`, :math:`\mu` can be determined by a bisection procedure.

In KSSOLV2.0, the regularized Kohn-Sham equation can be solved by the function :attr:`scf4m`.  The interaface for this function is identical to that of :attr:`scf`.  To perform a finite temperature calculation for a regularized model,

* we need to define the :attr:`temperature` attribute (in Kelvin) in the :attr:`Molecule` or :attr:`Crystal` input object. For example,

.. code-block:: matlab

   >> cry = Crystal('supercell',C, 'atomlist',atomlist, 'xyzlist' ,xyzlist, ...
      'ecutwfc',30, 'name','Cu', 'autokpts', autokpts, 'temperature', 300 );


* In addition, we need to initialize the :attr:`Wavefun` input object to have more than :math:`n_e` wavefunctions.

Note that we keep both :attr:`scf.m` and :attr:`scf4m.m` so that the difference between these two solvers can be easily compared, even though it is possible to use :attr:`scf4m.m` to perform a zero temperature calculation.
