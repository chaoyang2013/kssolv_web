Geometry optimization (atomic relaxation)
=========================================

KSSOLV can be used to optimize the atomic geometry of molecules
and solids by minimizing the total energy with respect to 
the positions of atoms.

Solving Kohn-Sham equations allows us to know the distribution of 
electrons in a system once the position of atoms is set. Since the
Hamiltonion in KSSLOV describes energy of the whole system, minimizing
the energy with resqect to nuclears' position can also tell us the
stationary position of atoms to construct a molecule. By doing so,
given a sset of atoms placing randomly, KSSOLV is able to find the
stationary position of atoms to form a molecule. The procedure can
be described as following loop ::

    while (force > tol)
        Do SCF iteration till convergence to get :math:`\rho`
        Get force by calculating gradient of energy with respect to nuclear position
        Optimize nuclear position using force
    end whlie

To use atomic relaxation in KSSLOV, we can use function ::

  >>[mol, H, X, info] = relaxatoms(mol, H, X, mdopt)

where mol is a previously constructed *Moleclue* boject, H is a 
previously set Hamiltonian, X is initial wafefunction of electrons
and mdopt is a structure includes all other option required in 
*relaxatoms* procedure, some of the important option is stated later.
The *relaxatoms* use scf to determine the wavefunction of electrons 
and sevearl optional methods to optimize the atoms' position as following:

************************************
**Optimization Toolbox**
************************************
This method use MATLAB optimization toolbox to finish the procedure.
To do this, just set :math:`mdopt.method=1` and it automatically 
calls the function ::

  >>[x,~] = fminunc(@(x) ksfg(x,mol,ksopts,int_cord), x0, optimopts)

Here, *ksfg* is a function returns the function value andgradient
of the objective optimization problem, *x0* is the initial point
 and optimopts is a structure including fields such as *Algorithm*
choosen form *quasi-newton* or *trust-region*, *MaxIterations*
indicating how many ierations the algorithm will run if the tolerance
criterian is not matched, *OptimalityTolerance* judging when the
local minima is found and other options, for more information about
*fminunc*, refer to `Documentaion<https://www.mathworks.com/help/optim/ug/fminunc.html>` on *Mathworks*

************************************
**Non-Linear Conjugate Gradient(NLCG)**
************************************  

*relaxatoms* supports NLCG method by setting :math:`mdopt.method=2` or
:math:`mdopt.method=3`, method=2 corresbonding to function *NLCG*, 
which requires options including *outiter_max* indicating the maxium
iteration number of NLCG if the tol is not reached, *ineriter_max* 
controls the inner ieration to find the proper step size for one
NLCG iteration, *tol1* and *tol2* represents the tolerance for
outer and inner iteration, *step_size* means the initial step size
for NLCG iteration. Method=3 corresbonding to function *nlcg*,
which requires two structure parameters *nlcg_pars* and *nlcg_opts*,
 *nlcg_pars* includes *nvar* meaning number of variable in the
optimization problem, *fgname* refering to a m-file returns
function value and gradient, *mol* and *ksopts* are the moleclue
structure and KSSOLV option defined in KSSOLV; *nlcg_opts* includes
initial point *x0*, number of maxium iteration *maxit*, tolerance
of gradient norm *normtol*, logic to determine whether to use
strong wolfe condition *strongwolfe* and parameter related to
wolfe condition *wolfe1* and *wolfe2* satisfying :math:`0<wolfe1<wolfe2<0.5`
for NLCG  

************************************
**BFGS method**
************************************  
*relaxatoms* supports BFGS mothod, a kind of quasi-newton method,
by setting :math:`mdopt.method=4` to call the *BFGS* function,
*BFGS* requires tow inputs, similar to *nlcg* method

************************************
**FIRE minimization**
************************************  
Fire minimization is a optimization problem for molecular dynamics,
and it corresbonds to :math:`method=5` in KSSOLV. This method calls
*quenchbyfirefuction*, and it requires two inputs *fire_pars* and
*fire_opts*; *fire_pars* includes fields such as  *pars_mass* 
indicating the atoms' mass (note that mass of all kinds of atoms are
set equal in FIRE), *fDec* and *fInc* inicating decreasment and
increasment of time step, *alpha* indicating the decrement of 
skier force compoenent acoef if projection dot(f,v) is positive
and *alphaStart* indicating coefficient of skier force update.
*fire_opts* includes some basic cofficients in optimization,
such as *dt* stands for time step, which can be seen as step size
in many algorithms, *MAXITER* stands for the maxium nember of iterations,
*TOL* stands for tolrance.
 
There are also many other tricks in Moleclue Dynamics

************************************
**Coordinate Transformation**
************************************  

In most cases, we use Cartesian coordinate to represent the positions of
atoms. However, this can lead to some ambiguity in geometry optimization
because the total energy and force are invariant with respect to a global 
translation and rotation of all atoms. Empirically, this ambiguity can also 
cause problems in molecular dynamics (MD). One way to solve such a problem is 
to use the so called internal coordinate representation which reparameterizes
the positions of atoms in terms of bond lengths, bond angles, and dihedral angles
among atoms. These internal coordinates can be conveniently represented by 
the so-called Z-matrix.

