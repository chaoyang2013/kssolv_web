
SCF Calculation of the Ground State of FCC Silicon
==================================================

This section shows how we can use KSSOLV2.0 to compute the ground state of
a FCC silicon crystal using a small unit cell and several k-points.

In the first part of this script, we set up the atomic species, and the unit cell, 
which is cubic and of size 5.108 Bohr. 

.. code-block:: matlab

   C = 5.108*eye(3);

The diamond cubic crystal structure has an FCC lattice with a basis of two 
silicon atoms positioned at the following coordinates

.. code-block:: matlab

   xyzlist = [
     0.0   0.0   0.0
     0.6   0.5   0.5
   ]*C;

6x6x6 Monkhorst k-points are chosen for the SCF calculation for the crystal.

.. code-block:: matlab

   autokpts = [ 6 6 6 0 0 0 ];

We set the planewave cutoff energy to 20.0 Hartree for this calculation.

.. code-block:: matlab

   cry = Crystal('supercell',C, 'atomlist',atomlist, 'xyzlist' ,xyzlist, ...
    'ecutwfc',20, 'name','Silicon', 'autokpts', autokpts );

We turn off verbose printing and set the maximum number SCF iterations allowed to 99.

.. code-block:: matlab

   opt = setksopt;
   opt.verbose = 'off';
   opt.maxscfiter = 99;

We perform the calculation by passing cry and opt to the scf function

.. code-block:: matlab

   [cry,H,X,info] = scf(cry,opt);

After 12 SCF iteration, convergence is reached (up to :math:`10^{-8}` default error tolerance in potential). 

.. code-block:: matlab

   Beging SCF4C calculation for Silicon...
   SCF iter   1:
   Rel Vtot Err    =            2.328e-02
   Total Energy    = -7.6995490732651e+00
   SCF iter   2:
   Rel Vtot Err    =            1.093e-02
   Total Energy    = -7.7029149725407e+00
   SCF iter   3:
   Rel Vtot Err    =            1.157e-03
   Total Energy    = -7.7039739814817e+00
   SCF iter   4:
   Rel Vtot Err    =            9.142e-04
   Total Energy    = -7.7040402053770e+00
   SCF iter   5:
   Rel Vtot Err    =            5.079e-04
   Total Energy    = -7.7040489780530e+00
   SCF iter   6:
   Rel Vtot Err    =            4.055e-05
   Total Energy    = -7.7040512445100e+00
   SCF iter   7:
   Rel Vtot Err    =            4.730e-05
   Total Energy    = -7.7040512415308e+00
   SCF iter   8:
   Rel Vtot Err    =            1.060e-05
   Total Energy    = -7.7040512601351e+00
   SCF iter   9:
   Rel Vtot Err    =            4.395e-07
   Total Energy    = -7.7040512611203e+00
   SCF iter  10:
   Rel Vtot Err    =            3.296e-07
   Total Energy    = -7.7040512611212e+00
   SCF iter  11:
   Rel Vtot Err    =            1.135e-08
   Total Energy    = -7.7040512611222e+00
   SCF iter  12:
   Rel Vtot Err    =            3.516e-09
   Total Energy    = -7.7040512611222e+00
   Convergence is reached!
   Etot            = -7.7040512611222e+00
   Entropy         =  0.0000000000000e+00
   Ekin            =  2.8394603907582e+00
   Eewald          = -1.1335649355718e+01
   Ealphat         =  0.0000000000000e+00
   Ecor            =  3.4915672601710e+00
   Ecoul           =  4.3091175109100e-02
   Exc             = -2.7425207314429e+00
   Efermi          =  7.4280740334052e-01
   --------------------------------------
   Total time used =            4.148e+02
   ||HX-XD||_F     =            5.206e-09
