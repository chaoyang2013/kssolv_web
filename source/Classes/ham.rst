
Ham
==================================

Ham is a class that defines a Kohn-Sham Hamiltonian associated with a
molecule.

Class Function List
----------------------

.. class:: Ham

   .. method:: Ham()

      Constructor that returns an empty :attr:`Ham` object.

   .. method:: Ham(mol)

      Constructor that returns a Hamiltonian object associated with the Molecule object :attr:`mol`. 
 
   .. method:: Ham(mol,rho)

      Constructor that returns a Hamiltonian object associated with the Molecule object :attr:`mol` and a specified electron density :attr:`rho`, which is a 3D array of floating point numbers.

Class Attribute List
----------------------

   .. attribute:: n1

      An integer that specifies the number of real space grid points in a corresponding wavefunction that the Hamiltonian can be applied to in the x direction.

   .. attribute:: n2

      An integer that specifies the number of real space grid points in a corresponding wavefunction that the Hamiltonian can be applied to in the y direction.

   .. attribute:: n3

      An integer that specifies the number of real space grid points in a corresponding wavefunction that the Hamiltonian can be applied to in the z direction.

   .. attribute:: gkin

      An array that contains Kinetic energy represented in Fourier reciprocal space for wavefunction

   .. attribute:: idxnz (same as that used in an :attr:`Wavefun` object)

      An array of integers that gives the 1D indices of the nonzero planewave coefficients (stored in :attr:`psi`) for each of the wavefunctions when it is reshaped from a 3D array to a 1D vector.

   .. attribute:: vtot

      A 3D array that contains the total potential (excluding the non-local ionic pseudopotential.)

   .. attribute:: vion

      A 3D array that contains the local ionic pseudopotential. 

   .. attribute:: vnp

      A 3D array that contains the charge density dependent potential

   .. attribute:: vnlmat

      A 2D array that contains the Kleinman-Bylander projector directions of non-local pseudopotentials 

   .. attribute:: vnlsign

      A 2D array that contains the Kleinman-Bylander projector scaling matrix of non-local pseudopotentials 

   .. attribute:: rho

      A 3D array that contains the electron density associated with the Hamiltonian.
       

