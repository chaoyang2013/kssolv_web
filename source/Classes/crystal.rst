
Crystal
==================================

:attr:`Crystal` is a class that defines a unit cell of a periodic system. This class is almost identical to the :attr:`Molecule` class except that it contains the k-point attribute.
 


Class Function List
----------------------

.. class:: Crystal

   .. method:: Crystal(key1,value1,key2,value2,...)

      Constructor for Crystal using (key, value) pairs. 
      Each key is a string, is followed by a constant (integer, float, string)
      In most cases, the values of the following keys need to be set 
      :attr:`supercell`, :attr:`atoms`, :attr:`alist`, :attr:`xyzlist`, 
      :attr:`ecutwfc`, :attr:`kpts`, :attr:`wks`. 
      We recommend setting the value of the 
      :attr:`name` key also. 

   .. function:: set(cry,key1,value1,key2,value2,...)

      Setting the attributes of a Crystal object :attr:cry using 
      (key,value) pairs specified in the arguments.

   .. function:: get(cry,key,value)

      Retrive an attribute of a Crystal object :attr:mol associated with
      a key.

   .. function:: finalize(cry)

      Reset all attributes of a Crystal object when some of the attributes have been reset.

Examples
--------

The following commands construct a FCC copper (Cu) molecule

.. code-block:: matlab

   >> a = Atom('C');

   >> atomlist = [a]; % atoms in the unit cell of a crystal

   % set up the unit cell
   >> C = [-3.411 0.000 3.411
            0.000 3.411 3.411
           -3.411 3.411 0.000];

   >> xyzlist = [0.0 0.0 0.0]; % atom position

   >> kpts = [
      0.0000  0.0000   0.0000
      0.0000  0.2500   0.0000
      0.0000  0.5000   0.0000
      0.0000  0.7500   0.0000
      0.2500  0.0000   0.0000
      0.2500  0.2500   0.0000
      0.2500  0.5000   0.0000
      0.2500  0.7500   0.0000
      0.5000  0.0000   0.0000
      0.5000  0.2500   0.0000
      0.5000  0.5000   0.0000
      0.5000  0.7500   0.0000
      0.7500  0.0000   0.0000
      0.7500  0.2500   0.0000
      0.7500  0.5000   0.0000
      0.7500  0.7500   0.0000
      0.0000  0.0000   0.2500
      0.0000  0.2500   0.2500
      0.0000  0.5000   0.2500
      0.0000  0.7500   0.2500
      0.2500  0.0000   0.2500
      0.2500  0.2500   0.2500
      0.2500  0.5000   0.0000
      0.2500  0.7500   0.0000
      0.5000  0.0000   0.0000
      0.5000  0.2500   0.0000
      0.5000  0.5000   0.0000
      0.5000  0.7500   0.0000
      0.7500  0.0000   0.0000
      0.7500  0.2500   0.0000
      0.7500  0.5000   0.0000
      0.7500  0.7500   0.0000
      0.0000  0.0000   0.2500
      0.0000  0.2500   0.2500
      0.0000  0.5000   0.2500
      0.0000  0.7500   0.2500
      0.2500  0.0000   0.2500
      0.2500  0.2500   0.2500
      0.2500  0.5000   0.2500
      0.2500  0.7500   0.2500
      0.5000  0.0000   0.2500
      0.5000  0.2500   0.2500
      0.5000  0.5000   0.2500
      0.5000  0.7500   0.2500
      0.7500  0.0000   0.2500
      0.7500  0.2500   0.2500
      0.7500  0.5000   0.2500
      0.7500  0.7500   0.2500
      0.0000  0.0000   0.5000
      0.0000  0.2500   0.5000
      0.0000  0.5000   0.5000
      0.0000  0.7500   0.5000
      0.2500  0.0000   0.5000
      0.2500  0.2500   0.5000
      0.2500  0.5000   0.5000
      0.2500  0.7500   0.5000
      0.5000  0.0000   0.5000
      0.5000  0.2500   0.5000
      0.5000  0.5000   0.5000
      0.5000  0.7500   0.5000
      0.7500  0.0000   0.5000
      0.7500  0.2500   0.5000
      0.7500  0.5000   0.5000
      0.7500  0.7500   0.5000
      0.0000  0.0000   0.7500
      0.0000  0.2500   0.7500
      0.0000  0.5000   0.7500
      0.0000  0.7500   0.7500
      0.2500  0.0000   0.7500
      0.2500  0.2500   0.7500
      0.2500  0.5000   0.7500
      0.2500  0.7500   0.7500
      0.5000  0.0000   0.7500
      0.5000  0.2500   0.7500
      0.5000  0.5000   0.7500
      0.5000  0.7500   0.7500
      0.7500  0.0000   0.7500
      0.7500  0.2500   0.7500
      0.7500  0.5000   0.7500
      0.7500  0.7500   0.7500 ];

   >> wks = 0.0156250 * ones(size(kpts,1),1);
   >> cry = Crystal('supercell',C, 'atomlist',atomlist, 'xyzlist' ,xyzlist, ...
           'ecutwfc',30.0, 'kpts', kpts, 'wks', wks, 'name','Cu' );


Instead of giving explicit coordinates of the k-points and there integration weights in wks, we can specify a uniform distribution of k-points in the unit cell (known as Monkhorst k-points) by specifying the :attr:`Monkhorst` attribute, with an array :attr:`[nkx nky nkz kx0 ky0 kz0] as its value, where :attr:`nkx`, :attr:`nky`, :attr:`nkz` represent the number of k-points in each reciprocal space lattice direction, and :attr:`kx0`, :attr:`ky0`, :attr:`kz0` are the offsets of the k-grid from the origin of the Brillouin zone.

.. code-block:: matlab

   >> cry = Crystal('supercell',C, 'atomlist',atomlist, 'xyzlist' ,xyzlist, ...
           'ecutwfc',30.0, 'Monkhorst', [6 6 6 0 0 0], 'name','Cu' );

Class Attribute List
----------------------

.. class:: Crystal

   .. attribute:: name 

      A string that specifies the name of a molecule.
 
   .. attribute:: supercell

      A 3x3 array that contains the Bravais lattice vectors used to specify a supercell containing the crystal.

   .. attribute:: alist

      An array that contains a list of atoms (object)

   .. attribute:: xyzlist

      An array that contains the x,y,z coordinates for each atom in the alist.

   .. attribute:: ecutwfc

      A floating point number that specifies the kinetic energy cutoff used in the planewave expansion.

   .. attribute:: ecutrho 

      A floating point number that specifies the energy cutoff used in the computation of the Hartree potential. If this attribute is not set, it is set to 4*ecutwfc by default.

   .. attribute:: n1 

      The number of grid points in the x direction.
      This is calculated by treating Ecut as the Nyquist sampling frequency.

      .. math:: n_1 = \frac{2E_{cut}}{2\pi/L_1}=\frac{E_{cut}\times L_1}{\pi},

      where :math:`L_1` is the unit cell size along the x direction.

   .. attribute:: n2 

      The number of grid points in the y direction.
      This is calculated by treating Ecut as the Nyquist sampling frequency.

      .. math:: n_2 = \frac{2E_{cut}}{2\pi/L_2}=\frac{E_{cut}\times L_2}{\pi}

      where :math:`L_2` is the unit cell size along the y direction.

   .. attribute:: n3 

      The number of grid points in the z direction.
      This is calculated by treating Ecut as the Nyquist sampling frequency.

      .. math:: n_3 = \frac{2E_{cut}}{2\pi/L_3}=\frac{E_{cut}\times L_3}{\pi}

      where :math:`L_3` is the unit cell size along the z direction.

   .. attribute:: kpts 

      An nk by 3 floating point array that gives the x, y, z coordinates of k-points in the Brillouin zone.
     
   .. attribute:: wks

      An nk by 1 floating point array that gives the integration weight of each k-point listed in the :attr:`kpts` attribute

   .. attribute:: autokpts

      An 6 by 1 floating point array that gives the number of k-points in each reciprocal lattice direction in the first three elements and the offset from the origin in the last three elements.  Uniformly space k-points (Monkhorst) are generated when this attribute is turned on.

   .. attribute:: gridwfc

   .. attribute:: gridrho

   .. attribute:: vext 

   .. attribute:: nspin 

      An integer that specifies spin type ( 1 = spin unpolarized, 2 = spin polarized)

   .. attribute:: temperature 

      A floating point number that specifies the temperature (in Calvin) used 
      in a finite temperature SCF calculation.

   .. attribute:: natoms 

      An integer that gives the number of atoms.

   .. attribute:: vol 

      A floating point number that gives the volume of the supercell

   .. attribute:: nel 

      An integer that gives the number of electrons (spin polarized) or electron pairs (spin unpolarized)

