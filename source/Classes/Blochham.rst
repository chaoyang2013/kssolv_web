.. _blochham_label:

BlochHam
==================================

:attr:`BlochHam` is a class that defines a Kohn-Sham Hamiltonian associated with a crystal (solid). This class is very similar to the :attr:`Ham` class. The key difference is that :attr:`BlochHam` includes quantities related to k-points, such as the number of k-points (:attr:`nkpts`). The non-local pseudopotential components are indexed by k-points.

Class Function List
----------------------

.. class:: BlockHam

   .. method:: BlockHam()

      Constructor that returns an empty :attr:`BlockHam` object.

   .. method:: BlockHam(crystal)

      Constructor that returns a Bloch Hamiltonian object associated with the Molecule object :attr:`crystal`. 
 
   .. method:: BlockHam(crystal,rho)

      Constructor that returns a Bloch Hamiltonian object associated with a :attr:`Crystal` object :attr:`crystal` and a specified electron density :attr:`rho`, which is a 3D array of floating point numbers.

Class Attribute List
----------------------

   .. attribute:: n1

      An integer that specifies the number of real space grid points in a corresponding wavefunction that the Hamiltonian can be applied to in the x direction.

   .. attribute:: n2

      An integer that specifies the number of real space grid points in a corresponding wavefunction that the Hamiltonian can be applied to in the y direction.

   .. attribute:: n3

      An integer that specifies the number of real space grid points in a corresponding wavefunction that the Hamiltonian can be applied to in the z direction.

   .. attribute:: nkpts

      An integer that specifies the number of k-points chosen in he Brillouin zone.

   .. attribute:: wks

      A floating point array that contains the weights associated with k-points.  This is the same as the weights defined in the corresponding :attr:`crystal`.


   .. attribute:: gkincell

      An array that contains Kinetic energies associated with differnt k-points.  

   .. attribute:: idxnz (same as that used in an :attr:`BlochWavefun` object)

      An array of integers that gives the 1D indices of the nonzero planewave coefficients (stored in :attr:`psi`) within a 3D array of size n1 by n2 by n3, for each of the wavefunctions. 

   .. attribute:: vtot

      A 3D array that contains the total potential (excluding the non-local ionic pseudopotential.)

   .. attribute:: vion

      A 3D array that contains the local ionic pseudopotential. 

   .. attribute:: vnp

      A 3D array that contains the charge density dependent potential

   .. attribute:: vnlmatcell

      A cell array that contains the Kleinman-Bylander (KB) form of non-local pseudopotentials for each k-point.

   .. attribute:: vnlsigncell

      A cell array that contains that scaling factors (matrix) for the KB form of non=local pseudopotential for each k-point.

   .. attribute:: vext

      A 3D array that contains a possibly user defined external potential.

   .. attribute:: rho

      A 3D array that contains the electron density associated with the Hamiltonian.
       
