Wavefun
==================================

Wavefun is a class that defines single-particle wavefunctions associated 
with valence electron pairs in a molecule.

Class Function List
----------------------

.. class:: Wavefun

   .. method:: Wavefun()

      Constructor that returns an empty :attr:`Wavefun` object.

   .. function:: Wavefun(n1,n2,n3)

      Constructor that returns a zero wave function of size n1 by n2 by n3 in real space

   .. function:: Wavefun(n1,n2,n3, ncols)

      Constructor that returns ncols zero wave functions, each of size n1 by n2 by n3 in real space

   .. function:: Wavefun(psi)

      Constructor that returns a :attr:`Wavefun` object that with planewave coefficients specified by the cell array :attr:`psi`.

   .. function:: Wavefun(psi,n1,n2,n3)
      
      Constructor that returns a :attr:`Wavefun` object that with planewave coefficients specified by the cell array :attr:`psi`, and of size n1 by n2 by n3 in reals space.

   .. function:: abs, fft3, ifft3, qr, svd, conj

      These functions are overloaded, and can be applied directly to a :attr:`Wavefun` object.  They typically returns another :attr:`Wavefun` object, or an :attr:`Wavefun` object as well as other matrices.

We can also add and subtract two :attr:`Wavefun` objects :attr:`X` and :attr:`Y` by simply writing :attr:`X+Y` or :attr:`X-Y`. We can transpose :attr:`X` by using :attr:`X.'` and conjugate transpose by using :attr:`X'`. The inner product of
:attr:`X` and :attr:`Y` can be simply written as :attr:`X'*Y`. The Hadamard product of :attr:`X` and :attr:`Y` can be written as :attr:`X*.Y`. The pointwise power of each element of :attr:`X` can be written as :attr:`X.^p`, where p is an integer or floating point number.

A subset of wavefunctions in :attr:`X` can be extracted by using MATLAB's subreference notation :attr:`X(:,i:j)`, where i <= j are indices that are less than or equal to the number of wavefunctions in :attr:`X`.

:attr:`Wavefun` objects can also be concatnated if their dimensions match, e.g.,
:attr:`Z = [X Y]`.



Class Attribute List
----------------------

   .. attribute:: n1

      An integer that specifies the number of real space grid points in the x direction.

   .. attribute:: n2

      An integer that specifies the number of real space grid points in the y direction.

   .. attribute:: n3

      An integer that specifies the number of real space grid points in the z direction.

   .. attribute:: nrows

      An integer that specifies the number of planewave coefficients for each of the wavefunctions.
  
   .. attribute:: ncols

      An integer that specifies the number of wavefunctions contained in the object.

   .. attribute:: idxnz

      An array of integers that gives the indices of the nonzero planewave coefficients (stored in :attr:`psi`) for each of the wavefunctions when it is reshaped from a 3D array to a 1D vector.

   .. attribute:: iscompact

      An integer that indicates whether only the nonzero planewave coefficients for each wavefunction are stored (in a compact format). It should be set to 1 if a compact is used, and 0 otherwise.

   .. attribute:: trans

      An integer that indicates whether the wavefunction itself (0) or the transpose of it is stored (1). 

   .. attribute:: occ

      An integer array that gives the occupation number for each of the stored wavefunction.
