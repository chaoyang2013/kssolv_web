Atom
==================================

Atom is a class that defines a number of attributes of an atom.

Class Function List
----------------------

.. class:: Atom

   .. function:: Atom(anum)

      Constructor for Atom based on the atom number `anum`. For example, a1 = Atom('Si') defines a silicon atom.

   .. function:: Atom(asym)

      Constructor for Atom based on the atom symbol `asym`. For example, a1 = Atom(14) defines a silicon atom.


Examples
--------

The following command construct a silicon atom object

.. code-block:: matlab

   >> a = Atom('Si')

The same object can be constructed by

.. code-block:: matlab

   >> a = Atom(14)

Multiple atoms can be placed in an array of atoms

   >> a1 = Atom('H');

   >> a2 = Atom('O'); 

   >> alist = [a1 a1 a2];

   >> for j = 1:2
   
   alist(j) = a1;

   end;

   >> alist(3) = a2;

Class Property List
----------------------

.. class:: Atom

   .. attribute:: symbol 

      The symbol of the atom, e.g., H, O, C, etc.

   .. attribute:: anum

      The atomic number of the atom.

   .. attribute:: amass

      The mass of the atom in atomic unit.

   .. attribute:: venum

      The number of valence electrons of the atom.

   .. attribute:: iloc 

   .. attribute:: occs 

   .. attribute:: occp 

   .. attribute:: occd 

   .. attribute:: iso 

   .. attribute:: ic 

   .. attribute:: isref 

   .. attribute:: ipref 

   .. attribute:: idref 

