
Ggrid
==================================

Ggrid is a class that keeps track of the set of reciprocal space grid 
points defined by the user defined kinetic energy cutoff parameter :math:`E_{cut}`.  

KSSOLV works with reciprocal space grid points that are inside a sphere of
radius :math:`r = \sqrt{\mathrm{Ecut}^2/2}`.

Class Function List
----------------------

.. class:: Molecule

   .. method:: Ggrid()

      Constructor that returns an empty :attr:`Ggrid` object.

   .. method:: Ggrid(mol)

      Constructor that returns a set of reciprocal space grid points associated with a unit (super)cell defined in the :attr:`mol` object.  These grid points satisfy the energy cutoff :math:`E_{cut}` specified in :attr:`mol` also.

   .. method:: Ggrid(mol,ecut)

      Constructor that returns a set of reciprocal space grid points associated with a unit (super)cell defined in the :attr:`mol` object.  These grid points satisfy the energy cutoff :math:`E_{cut}` specified as an input.

   .. method:: Ggrid(gx,gy,gz,ecut)

      Constructor that returns a set of reciprocal space grid points defined by :attr:`gx`, :attr:`gy`, :attr:`gz` and satisfy the :math:`E_{cut}` constraint.

   .. method:: val = get(ggrid,key)

      A function that returns the value of an attribute of a :attr:`Ggrid` object :attr:`ggrid`. The name of the attribute is specified by a key string which is defined below.

Class Attribute List
----------------------

   .. attribute:: ecut 

      A floating point number that gives the energy cutoff used to define the valid reciprocal space grid points.

   .. attribute:: ng

      An integer that gives the number of valid reciprocal space grid points within the :attr:`ecut` cutoff 

   .. attribute:: idxnz

      A array that gives the index of each valid reciprocal space grid point in the 3D array that contains all reciprocal space points.  This array is also used for a :attr:`Wavefun` object.

   .. attribute:: gkk

      A array that gives the magnitude square of each valid reciprocal space grid point.

   .. attribute:: gkx

      A array that gives the x-coordinate of each valid reciprocal space grid point.

   .. attribute:: gky

      A array that gives the y-coordinate of each valid reciprocal space grid point.

   .. attribute:: gkz

      A array that gives the z-coordinate of each valid reciprocal space grid point.
