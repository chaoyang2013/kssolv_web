
Molecule
==================================

Molecule is a class that defines a number of attributes of a molecule. 

Class Function List
----------------------

.. class:: Molecule

   .. method:: Molecule(key1,value1,key2,value2,...)

      Constructor for Molecule using (key, value) pairs. 
      Each key is a string, is followed by a constant (integer, float, string)
      In most cases, the values of the following keys need to be set 
      :attr:`supercell`, :attr:`atoms`, :attr:`alist`, :attr:`xyzlist`, 
      :attr:`ecutwfc`. We recommend setting the value of the 
      :attr:`name` key also. A typical example of the
      constructor can be found in :ref:`setup_molecule`.

   .. function:: set(mol,key1,value1,key2,value2,...)

      Setting the attributes of a Molecule object :attr:mol using 
      (key,value) pairs specified in the arguments.

   .. function:: get(mol,key,value)

      Retrive an attribute of a Molecule object :attr:mol associated with
      a key.

   .. function:: finalize(mol)

      Reset all attributes of a Molecule object when some of the attributes have been reset.

Examples
--------

The following commands construct a hydrogen molecule

.. code-block:: matlab

   >> a = Atom('H');

   >> atomlist = [a a]; % atoms in the molecule

   >> C = 10*eye(3); % the supercell that contains the molecule

   >> xyzlist = [1.5 0.0 0.0; 0.0 0.0 0.0]; % atom positions 

   >> mol = Molecule('supercell',C, 'atomlist',atomlist, 'xyzlist' ,xyzlist, ...
           'ecutwfc',100.0, 'name','h2' )

.. code-block:: matlab

Class Attribute List
----------------------

.. class:: Molecule

   .. attribute:: name 

      A string that specifies the name of a molecule.
 
   .. attribute:: supercell

      A 3x3 array that contains the Bravais lattice vectors used to specify a supercell containing the Molecule.

   .. attribute:: alist

      An array that contains a list of atoms (object)

   .. attribute:: xyzlist

      An array that contains the x,y,z coordinates for each atom in the alist.

   .. attribute:: ecutwfc

      A floating point number that specifies the kinetic energy cutoff used in the planewave expansion.

   .. attribute:: ecutrho 

      A floating point number that specifies the energy cutoff used in the computation of the Hartree potential. If this attribute is not set, it is set to 4*ecutwfc by default.

   .. attribute:: n1 

      The number of grid points in the x direction.
      This is calculated by treating Ecut as the Nyquist sampling frequency.

      .. math:: n_1 = \frac{2E_{cut}}{2\pi/L_1}=\frac{E_{cut}\times L_1}{\pi},

      where :math:`L_1` is the unit cell size along the x direction.

   .. attribute:: n2 

      The number of grid points in the y direction.
      This is calculated by treating Ecut as the Nyquist sampling frequency.

      .. math:: n_2 = \frac{2E_{cut}}{2\pi/L_2}=\frac{E_{cut}\times L_2}{\pi}

      where :math:`L_2` is the unit cell size along the y direction.

   .. attribute:: n3 

      The number of grid points in the z direction.
      This is calculated by treating Ecut as the Nyquist sampling frequency.

      .. math:: n_3 = \frac{2E_{cut}}{2\pi/L_3}=\frac{E_{cut}\times L_3}{\pi}

      where :math:`L_3` is the unit cell size along the z direction.

   .. attribute:: gridwfc

   .. attribute:: gridrho

   .. attribute:: vext 

   .. attribute:: nspin 

      An integer that specifies spin type ( 1 = spin unpolarized, 2 = spin polarized)

   .. attribute:: temperature 

      A floating point number that specifies the temperature (in Calvin) used 
      in a finite temperature SCF calculation.

   .. attribute:: natoms 

      An integer that gives the number of atoms.

   .. attribute:: vol 

      A floating point number that gives the volume of the supercell

   .. attribute:: nel 

      An integer that gives the number of electrons (spin polarized) or electron pairs (spin unpolarized)

